<?php
$style = '';
if ( ! is_front_page() ) :
	$style = 'no_home';
endif;
?>
<!-- Begin Logo -->
	<section class="logo <?php echo $style; ?> wow bounceInUp" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
		</div>
	</section>
<!-- End Logo -->